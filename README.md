# P&O Test Server

Please contact <thomas.winant@cs.kuleuven.be> when you encounter bugs or have
any questions.


## Dependencies

- Python2
- virtualenv

## Installation

* `cd` to the top-level directory containing this file, the server folder,
  world and world2.
* `virtualenv .`
* `bin/pip install tornado nose requests`

Depending on your Python version, you might have to invoke `virtualenv2` and
`bin/pip2`.

Alternatively, you can install the tornado, nose, and requests Python (2.7)
libraries via your package manager, in which case you don't need virtualenv
(you can also drop the bin/ prefix in this case).


## Running

Invoke the server in the top-level directory as such:

    bin/python server/server.py

Pass `--port=8080` if you wish to override the default port.

Depending on your Python version, you might have to invoke `bin/python2`.

## Testing

Invoke the testsuite in the top-level directory as such:

    bin/nosetests

The server doesn't have to be running to run the testsuite.

## Emulate activity

To emulate some activity on the server, invoke the following script while the
server is running.

    bin/python server/emulate.py

This script will periodically update the positions of all robots.

Stop it with `Ctrl-C`.

Pass `--host=192.168.1.*` if you're not using the default host (`localhost`)
and `--port=8080` if you're not using the default port (`4000`).
