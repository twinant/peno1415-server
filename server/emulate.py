# Simulates activity on the server.
# Run from the root as:
# $ bin/python2 server/emulate.py
# Optional arguments:
# --host=localhost --port=4000


import random
import time
import requests
import tornado.options

from tornado.options import define, options


## Command line options, e.g. --host=localhost
define("host", default="localhost", type=str,
       help="the host on which the server runs")
define("port", default=4000, type=int,
       help="the port on which the server runs")


teams = ["paars", "groen", "indigo", "goud", "blauw", "brons", "koper",
         "ijzer", "rood", "geel", "zilver", "platinum"]
width = 5
height = 6

# A map from team to a 2D-coordinate (int, int)
positions = {}
# A map from team to an orientation [NSWE]
orientations = {}

def random_orientation():
    r = random.randint(1, 4)
    # Uggh
    if r == 1:
        return 'N'
    elif r == 2:
        return 'S'
    elif r == 3:
        return 'E'
    elif r == 4:
        return 'W'

if __name__ == "__main__":
    tornado.options.parse_command_line()
    for team in teams:
        positions[team] = (random.randint(0, width), random.randint(0, height))
        orientations[team] = random_orientation()

    while True:
        team = random.choice(teams)
        (x, y) = positions[team]
        direction = random.randint(1, 4)
        if direction == 1:
            x = min(width, x + 1)
        elif direction == 2:
            x = max(0, x - 1)
        elif direction == 3:
            y = min(height, y + 1)
        elif direction == 4:
            y = max(0, y - 1)

        positions[team] = (x, y)

        orientation = orientations[team]

        if random.randint(1, 2) == 1:
            orientation = random_orientation()
            orientations[team] = orientation

        requests.post("http://{}:{}/position/{}"
                      .format(options.host, options.port, team),
                      "{}{}".format(x * width + y, orientation))

        time.sleep(0.5)
