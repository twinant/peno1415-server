import re

import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web

from tornado.options import define, options

from either import Left, Right
from datatypes import Location, Symbol, GuessResult, EventType
from parser import parse_map


## Command line options, e.g. --port=4000
define("port", default=4000, help="run on the given port", type=int)
define("map", default="world", help="use the given map file", type=str)


## For validation

teams = set(["paars", "groen", "indigo", "goud", "blauw", "brons", "koper",
             "ijzer", "rood", "geel", "zilver", "platinum"])

colors = set(["red", "purple", "yellow", "green", "blue"])

shapes = set(["square", "triangle", "circle", "star"])


## Helpers

# Return Left(location: Location) in case of a valid location, otherwise
# Right(error message: String). TODO we don't distinguish invalid orientations
# from invalid positions.
def validate_location(location, unknown_allowed=True):
    if location == "??" and unknown_allowed:
        return Right(Location('?', '?'))
    match = re.match("(\d+)([NSWE])", location)
    if match:
        (positionStr, orientation) = match.groups()
        position = int(positionStr)
        if state.world_map.in_map(position):
            return Right(Location(position, orientation))
    return Left("Invalid position: {}".format(location))


def log_event(msg, event_type=EventType.Info):
    # For now
    print(msg)


## Custom handler

class APIRequestHandler(tornado.web.RequestHandler):
    def set_default_headers(self):
        self.set_header("Content-Type", "text/plain")
        self.set_header('Access-Control-Allow-Origin', '*')


## Routes

class IndexHandler(APIRequestHandler):
    def get(self):
        self.write("PenO")


class WorldHandler(APIRequestHandler):
    def initialize(self, state):
        self.state = state

    def get(self):
        with open(self.state.map_file, 'r') as f:
            for line in f:
                match = re.match("\?(\d+).+", line)
                if match:
                    self.write("{}?\n".format(match.group(1)))
                else:
                    self.write(line)


class PositionHandler(APIRequestHandler):
    def initialize(self, state):
        self.state = state

    def get(self, team):
        if team == "all":
            self.write("{}\n".format(
                ' '.join([k + str(loc)
                          for k, loc
                          in self.state.robots.iteritems()])))
        elif team in teams:
            self.write("{}\n".format(
                self.state.robots.get(team, Location.Unknown())))
        else:
            self.set_status(400)
            self.write("Invalid team: {}\n".format(team))

    def post(self, team):
        if team in teams:
            either_location = validate_location(self.request.body)
            if either_location.is_left():
                self.set_status(400)
                self.write("{}\n".format(either_location.value()))
            else:
                location = either_location.value()
                if location.is_unknown():
                    if team in self.state.robots:
                        del self.state.robots[team]
                else:
                    self.state.robots[team] = location
                log_event("{} now at {}".format(team, location))
                self.write("OK\n")
        else:
            self.set_status(400)
            self.write("Invalid team: {}\n".format(team))


class SymbolHandler(APIRequestHandler):
    def initialize(self, state):
        self.state = state

    def get(self, path):
        if path == "all":
            self.write("{}\n".format(
                ' '.join(["{}{}{}_{}".format(
                    i, tag.orientation, tag.symbol.color, tag.symbol.shape)
                          for (i, tag)
                          in self.state.world_map.discovered_tags()])))
            return
        else:
            self.set_status(404)
            self.write("not found\n")
            return

    def post(self, path):
        team = self.request.headers.get("User-Agent", "")

        if team not in teams:
            self.set_status(400)
            self.write("Invalid team: {}\n".format(team))
            return

        # Any team that made a request with a valid user agent should be
        # included in the score board
        if team not in self.state.scores:
            self.state.scores[team] = 0.0

        either_location = validate_location(path, unknown_allowed=False)
        if either_location.is_left():
            self.set_status(400)
            self.write("{}\n".format(either_location.value()))
            return
        location = either_location.value()

        print(self.request.body)
        match = re.match("(.+?)_(.+)", self.request.body)
        if not match or match.group(1) not in colors:
            self.set_status(400)
            self.write("Invalid color: {}\n".format(
                match.group(1) if match else self.request.body))
            return

        (color, shape) = match.groups()
        if shape not in shapes:
            self.set_status(400)
            self.write("Invalid shape: {}\n".format(shape))
            return

        symbol = Symbol(color, shape)

        if not self.state.world_map.unknown_symbol_at(location):
            self.set_status(400)
            log_event("{} thought {} was at {}".format(team, symbol, location),
                      event_type=EventType.Wrong)
            self.write("No symbol at: {}\n".format(location))
            return

        robot_location = self.state.robots.get(team, Location.Unknown())
        if robot_location.position != location.position:
            self.set_status(400)
            log_event("{} found symbol {} at {} but still on tile {}"
                      .format(team, symbol, location.position,
                              robot_location.position),
                      event_type=EventType.Wrong)
            self.write("Robot not on tile: "
                       "on {} but should be on "
                       "{}\n".format(
                           robot_location.position, location.position))
            return

        result = self.state.world_map.guess(location, team, symbol)
        if result == GuessResult.FirstTry:
            self.state.scores[team] += 1.0
            log_event("{} discovered {} at {} for 1.0 points"
                      .format(team, symbol, location),
                      event_type=EventType.Correct)
            self.write("1.0\n")
        elif result == GuessResult.SecondTry:
            self.state.scores[team] += 0.5
            log_event("{} discovered {} at {} for 0.5 points"
                      .format(team, symbol, location),
                      event_type=EventType.Correct)
            self.write("0.5\n")
        elif result == GuessResult.NthTry:
            log_event("{} discovered {} at {} for 0.0 points"
                      .format(team, symbol, location),
                      event_type=EventType.Correct)
            self.write("0.0\n")
        elif result == GuessResult.Wrong:
            log_event("{} guessed wrong: {} not at {}"
                      .format(team, symbol, location),
                      event_type=EventType.Wrong)
            self.write("WRONG\n")
        else:
            log_event("{} was too late: symbol at {} already discovered"
                      .format(team, location),
                      event_type=EventType.Correct)
            self.write("TOOLATE\n")


class ScoreHandler(APIRequestHandler):
    def initialize(self, state):
        self.state = state

    def get(self):
        self.write("{}\n".format(
            ' '.join(["{}{:.1f}".format(team, score)
                      for team, score in self.state.scores.iteritems()])))


## State
class State():
    def __init__(self, map_file):
        # A map from team to points (float)
        self.scores = {}
        # A map from team to location (Location)
        self.robots = {}
        # The name of the map file
        self.map_file = map_file
        # The map, which includes the discovered tags
        self.world_map = parse_map(map_file)
        # A map from IP addresses to teams, to detect cheaters
        self.clients = {}
        # A list of events
        self.events = []

## Eww, global state
state = None


## The App
def app(map_file=None):
    tornado.options.parse_command_line()
    global state
    state = State(map_file or options.map)
    return tornado.web.Application(handlers=[
        (r"/", IndexHandler),
        (r"/world", WorldHandler, dict(state=state)),
        (r"/position/(.+)", PositionHandler, dict(state=state)),
        (r"/symbol/(.+)", SymbolHandler, dict(state=state)),
        (r"/score/all", ScoreHandler, dict(state=state))
    ], autoreload=True)


## Run the App as an HTTP server
if __name__ == "__main__":
    http_server = tornado.httpserver.HTTPServer(app())
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()
