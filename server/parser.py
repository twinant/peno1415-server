from datatypes import Map, Tag, Tile, Symbol, TagState
import re

def strip_comments(line):
    return re.sub("\s*#.+", "", line)

def parse_dimensions(lines):
    match = re.match("(\d+)\s+(\d+)", lines[0])
    if match:
        return (int(match.group(1)), int(match.group(2)), lines[1:])
    raise ValueError("Couldn't find world dimensions")


def parse_tile(token):
    match = re.match("(\d+)([NSWE])", token)
    if match:
        return Tile(int(match.group(1)), match.group(2), None)
    raise ValueError("Couldn't parse tile: {}".format(token))


def parse_tag(token):
    match = re.match("(\?)?(\d+)([NSWE]).*?"
                     "(red|purple|yellow|green|blue).*?"
                     "(square|triangle|circle|star).+", token)
    if match:
        (unknown, position, orientation, color, shape) = match.groups()
        state = TagState.Unknown if unknown == "?" else TagState.Known
        return (int(position), Tag(orientation, Symbol(color, shape), state))
    raise ValueError("Couldn't parse tag: {}".format(token))


def parse_map(file):
    with open(file, 'r') as f:
        lines = f.readlines()
    without_comments = [strip_comments(line) for line in lines]
    # Split on whitespace
    tokenised = sum([re.split("\s+", line) for line in without_comments], [])
    # Remove empty tokens
    tokens = filter(lambda token: not token.strip() == "", tokenised)
    if len(tokens) < 2:
        raise ValueError("parse error: less than 2 tokens")

    (widthStr, heightStr) = tokens[0:2]
    width = int(widthStr)
    height = int(heightStr)
    tiles = [parse_tile(token) for token in tokens[2:2 + width * height]]
    tags = [parse_tag(token) for token in tokens[2 + width * height:]]

    for (position, tag) in tags:
        tiles[position].tag = tag

    return Map(width, height, tiles)
