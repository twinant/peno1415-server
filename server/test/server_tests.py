import tornado.testing
from server.server import app, state
from nose.tools import assert_equal, assert_in

class IndexTest(tornado.testing.AsyncHTTPTestCase):
    def get_app(self):
        return app()

    def test_index(self):
        r = self.fetch("/")
        assert_equal(r.code, 200)
        assert_in("PenO", r.body)

class PositionTest(tornado.testing.AsyncHTTPTestCase):
    def get_app(self):
        return app()

    def test_position(self):
        r = self.fetch("/position/all")
        assert_equal(r.code, 200)
        assert_equal(r.body, "\n")

        r = self.fetch("/position/")
        assert_equal(r.code, 404)
        assert_in("Not Found", r.body)

        r = self.fetch("/position/foo")
        assert_equal(r.code, 400)
        assert_equal(r.body, "Invalid team: foo\n")

        r = self.fetch("/position/rood")
        assert_equal(r.code, 200)
        assert_equal(r.body, "??\n")

        r = self.fetch("/position/rood", method="POST", body="50E")
        assert_equal(r.code, 400)
        assert_equal(r.body, "Invalid position: 50E\n")

        r = self.fetch("/position/rood", method="POST", body="10U")
        assert_equal(r.code, 400)
        # TODO should be orientation
        assert_equal(r.body, "Invalid position: 10U\n")

        r = self.fetch("/position/rood", method="POST", body="10N")
        assert_equal(r.code, 200)
        assert_equal(r.body, "OK\n")

        r = self.fetch("/position/rood")
        assert_equal(r.code, 200)
        assert_equal(r.body, "10N\n")

        r = self.fetch("/position/all")
        assert_equal(r.code, 200)
        assert_equal(r.body, "rood10N\n")

        r = self.fetch("/position/blauw", method="POST", body="3E")
        assert_equal(r.code, 200)
        assert_equal(r.body, "OK\n")

        r = self.fetch("/position/all")
        assert_equal(r.code, 200)
        assert_equal(r.body, "blauw3E rood10N\n")

        r = self.fetch("/position/blauw", method="POST", body="??")
        assert_equal(r.code, 200)
        assert_equal(r.body, "OK\n")

        r = self.fetch("/position/all")
        assert_equal(r.code, 200)
        assert_equal(r.body, "rood10N\n")

        r = self.fetch("/position/rood", method="POST", body="??")
        assert_equal(r.code, 200)
        assert_equal(r.body, "OK\n")

        r = self.fetch("/position/all")
        assert_equal(r.code, 200)
        assert_equal(r.body, "\n")

        r = self.fetch("/position/rood")
        assert_equal(r.code, 200)
        assert_equal(r.body, "??\n")

class SymbolTest(tornado.testing.AsyncHTTPTestCase):
    def get_app(self):
        return app("world2")

    def test_position(self):
        r = self.fetch("/symbol/all")
        assert_equal(r.code, 200)
        assert_equal(r.body, "\n")

        r = self.fetch("/symbol/10E", method="POST", body="")
        assert_equal(r.code, 400)
        assert_equal(r.body, "Invalid team: \n")

        r = self.fetch("/symbol/10E", method="POST", body="", user_agent="Firefox")
        assert_equal(r.code, 400)
        assert_equal(r.body, "Invalid team: Firefox\n")

        r = self.fetch("/symbol/100E", method="POST", body="", user_agent="blauw")
        assert_equal(r.code, 400)
        assert_equal(r.body, "Invalid position: 100E\n")

        r = self.fetch("/symbol/10U", method="POST", body="", user_agent="blauw")
        assert_equal(r.code, 400)
        # TODO should be orientation
        assert_equal(r.body, "Invalid position: 10U\n")


        r = self.fetch("/symbol/10E", method="POST", body="fuchsia_square",
                       user_agent="blauw")
        assert_equal(r.code, 400)
        assert_equal(r.body, "Invalid color: fuchsia\n")


        r = self.fetch("/symbol/10E", method="POST", body="red_pentagram",
                       user_agent="blauw")
        assert_equal(r.code, 400)
        assert_equal(r.body, "Invalid shape: pentagram\n")


        r = self.fetch("/symbol/10E", method="POST", body="red_square",
                       user_agent="blauw")
        assert_equal(r.code, 400)
        assert_equal(r.body, "No symbol at: 10E\n")

        r = self.fetch("/symbol/12W", method="POST", body="purple_star",
                       user_agent="blauw")
        assert_equal(r.code, 400)
        assert_equal(r.body, "Robot not on tile: on ? but should be on 12\n")

        r = self.fetch("/position/blauw", method="POST", body="12E")
        r = self.fetch("/symbol/12W", method="POST", body="purple_star",
                       user_agent="blauw")
        assert_equal(r.code, 200)
        assert_equal(r.body, "1.0\n")

        r = self.fetch("/symbol/all")
        assert_equal(r.code, 200)
        assert_equal(r.body, "12Wpurple_star\n")

        r = self.fetch("/position/rood", method="POST", body="12W")
        r = self.fetch("/symbol/12W", method="POST", body="purple_star",
                       user_agent="rood")
        assert_equal(r.code, 200)
        assert_equal(r.body, "TOOLATE\n")

        r = self.fetch("/position/blauw", method="POST", body="31S")
        r = self.fetch("/symbol/31N", method="POST", body="red_square",
                       user_agent="blauw")
        assert_equal(r.code, 200)
        assert_equal(r.body, "WRONG\n")

        r = self.fetch("/symbol/31N", method="POST", body="red_circle",
                       user_agent="blauw")
        assert_equal(r.code, 200)
        assert_equal(r.body, "0.5\n")

        r = self.fetch("/position/blauw", method="POST", body="18W")
        r = self.fetch("/symbol/18W", method="POST", body="purple_square",
                       user_agent="blauw")
        assert_equal(r.code, 200)
        assert_equal(r.body, "WRONG\n")

        r = self.fetch("/symbol/18W", method="POST", body="red_square",
                       user_agent="blauw")
        assert_equal(r.code, 200)
        assert_equal(r.body, "WRONG\n")

        r = self.fetch("/symbol/18W", method="POST", body="purple_triangle",
                       user_agent="blauw")
        assert_equal(r.code, 200)
        assert_equal(r.body, "0.0\n")

        r = self.fetch("/position/rood", method="POST", body="18E")
        r = self.fetch("/symbol/18W", method="POST", body="purple_triangle",
                       user_agent="rood")
        assert_equal(r.code, 200)
        assert_equal(r.body, "1.0\n")

        r = self.fetch("/symbol/18W", method="POST", body="purple_triangle",
                       user_agent="blauw")
        assert_equal(r.code, 200)
        assert_equal(r.body, "TOOLATE\n")


class ScoreTest(tornado.testing.AsyncHTTPTestCase):
    def get_app(self):
        return app("world2")

    def test_score(self):
        r = self.fetch("/score/all")
        assert_equal(r.code, 200)
        assert_equal(r.body, "\n")

        r = self.fetch("/position/blauw", method="POST", body="12E")
        r = self.fetch("/symbol/12W", method="POST", body="purple_star",
                       user_agent="blauw")
        assert_equal(r.code, 200)
        assert_equal(r.body, "1.0\n")

        r = self.fetch("/score/all")
        assert_equal(r.code, 200)
        assert_equal(r.body, "blauw1.0\n")

        r = self.fetch("/position/paars", method="POST", body="12E")
        r = self.fetch("/symbol/0W", method="POST", body="purple_star",
                       user_agent="paars")
        assert_equal(r.code, 400)
        assert_equal(r.body, "No symbol at: 0W\n")

        r = self.fetch("/score/all")
        assert_equal(r.body, "blauw1.0 paars0.0\n")

        r = self.fetch("/position/blauw", method="POST", body="18E")
        r = self.fetch("/symbol/18W", method="POST", body="purple_square",
                       user_agent="blauw")
        assert_equal(r.body, "WRONG\n")

        r = self.fetch("/symbol/18W", method="POST", body="purple_triangle",
                       user_agent="blauw")
        assert_equal(r.body, "0.5\n")

        r = self.fetch("/score/all")
        assert_equal(r.body, "blauw1.5 paars0.0\n")
