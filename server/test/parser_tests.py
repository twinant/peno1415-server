from server.parser import parse_map
from server.datatypes import TagState
from nose.tools import assert_equal, assert_is_none, assert_is_not_none

def test_parse_map():
    map = parse_map("world2")
    assert_equal(map.width, 6)
    assert_equal(map.height, 6)
    assert_equal(len(map.tiles), 36)
    ts = [3, 1, 3, 3, 1, 3, 2, 2, 3, 3, 1, 1, 3, 0, 2, 1, 1, 2, 3, 1, 3, 0, 0,
          1, 2, 4, 2, 2, 1, 3, 3, 2, 3, 3, 3, 3]
    os = ['N', 'N', 'E', 'N', 'N', 'E', 'S', 'S', 'W', 'S', 'W', 'E', 'W',
          'N', 'W', 'N', 'E', 'S', 'N', 'E', 'N', 'N', 'N', 'E', 'N', 'S',
          'S', 'S', 'W', 'S', 'W', 'E', 'S', 'W', 'S', 'N']
    for i in xrange(36):
        assert_equal(map.tiles[i].tile_type, ts[i])
        assert_equal(map.tiles[i].orientation, os[i])

    unknown = [12, 18, 31]
    tags = {1: ('N', 'red', 'square'),
            4: ('N', 'purple', 'square'),
            5: ('E', 'yellow', 'triangle'),
            9: ('E', 'green', 'circle'),
            12: ('W', 'purple', 'star'),
            15: ('N', 'red', 'star'),
            16: ('E', 'blue', 'circle'),
            18: ('W', 'purple', 'triangle'),
            20: ('W', 'yellow', 'circle'),
            29: ('E', 'green', 'square'),
            31: ('N', 'red', 'circle'),
            34: ('E', 'blue', 'square')}

    for i in xrange(36):
        tag = map.tiles[i].tag
        if i in tags.viewkeys():
            assert_is_not_none(tag)
            if i in unknown:
                assert_equal(tag.state, TagState.Unknown)
            else:
                assert_equal(tag.state, TagState.Known)
            (orientation, color, shape) = tags[i]
            assert_equal(tag.orientation, orientation)
            assert_equal(tag.symbol.color, color)
            assert_equal(tag.symbol.shape, shape)
        else:
            assert_is_none(tag)
