
class EventType:
    Debug, Info, Suspicious, Wrong, Correct = range(5)

class Symbol:
    def __init__(self, color, shape):
        self.color = color
        self.shape = shape

    def __eq__(self, other):
        return self.color == other.color and self.shape == other.shape

    def __str__(self):
        return "{}_{}".format(self.color, self.shape)

class TagState:
    Known, Unknown, Discovered = range(3)

# A Tag is an orientation + a symbol + the state of the symbol
class Tag:
    # state is a TagState
    def __init__(self, orientation, symbol, state):
        self.orientation = orientation
        self.symbol = symbol
        self.state = state
        # self.guesses is a dict from team names to ints representing the
        # number of guesses
        self.guesses = {}

class Tile:
    def __init__(self, tile_type, orientation, tag):
        self.tile_type = tile_type
        self.orientation = orientation
        # Can be None
        self.tag = tag

# A location is a position (0-based index of a tile) + an orientation
class Location:
    def __init__(self, position, orientation):
        self.position = position
        self.orientation = orientation

    def __str__(self):
        return "{}{}".format(self.position, self.orientation)

    def is_unknown(self):
        return self.position == '?' or self.orientation == '?'

    # FIXME make a singleton
    @staticmethod
    def Unknown():
        return Location('?', '?')



class GuessResult:
    FirstTry, SecondTry, NthTry, Wrong, TooLate = range(5)

class Map:
    def __init__(self, width, height, tiles):
        self.width = width
        self.height = height
        self.tiles = tiles

    # Return True when the given position is in the map.
    def in_map(self, position):
        return 0 <= position < self.width * self.height

    # Return True when there is an unknown symbol, discovered or not, at the
    # given location. Assumes the position is in the map (use `in_map`).
    def unknown_symbol_at(self, location):
        tag = self.tiles[location.position].tag
        return tag and tag.state != TagState.Known and \
            tag.orientation == location.orientation

    # Return an indexed list (format: (position: i, tag: Tag)) of the tags of
    # which the symbol is discovered.
    def discovered_tags(self):
        return [(i, tile.tag) for i, tile in enumerate(self.tiles)
                if tile.tag and tile.tag.state == TagState.Discovered]

    # Let a `team` guess the `symbol` located at `location` in the map.
    # Assumes `self.unknown_symbol_at(location)` is `True`, so there must be
    # an unknown (but maybe already discovered) symbol at `location`. If it's
    # already discovered, return `GuessResult.TooLate`. If the guess is
    # incorrect, increment the team's number of guesses for the tile and
    # return `GuessResult.Wrong`. If the guess is correct, return
    # `GuessResult.{First,Second,Nth}Try` depending on the number of guesses
    # made before.
    def guess(self, location, team, symbol):
        tag = self.tiles[location.position].tag

        # Already discovered
        if tag.state == TagState.Discovered:
            return GuessResult.TooLate
        # Not yet discovered
        elif tag.state == TagState.Unknown:
            guesses = tag.guesses.get(team, 0)
            # Correct
            if tag.symbol == symbol:
                if guesses == 0:
                    tag.state = TagState.Discovered
                    return GuessResult.FirstTry
                elif guesses == 1:
                    tag.state = TagState.Discovered
                    return GuessResult.SecondTry
                else:
                    # We don't set the state to discovered, because otherwise
                    # a team could try to guess all symbols until they're
                    # discovered, after which there will be no symbols left
                    # for the other teams to discover.
                    return GuessResult.NthTry
            # Wrong
            else:
                tag.guesses[team] = guesses + 1
                return GuessResult.Wrong

    # Reset all discovered tags to unknown and clear the guesses.
    def clear(self):
        for tile in self.tiles:
            tag = tile.tag
            if tag and tag.state == TagState.Discovered:
                tag.guesses.clear()
                tag.state = TagState.Unknown
